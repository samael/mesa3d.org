---
title:    "Mesa 11.1.3 and Mesa 11.2.1 are released"
date:     2016-04-17
category: releases
tags:     []
---
[Mesa 11.1.3](https://docs.mesa3d.org/relnotes/11.1.3.html) and [Mesa
11.2.1](https://docs.mesa3d.org/relnotes/11.2.1.html) are released. These are bug-fix releases
from the 11.1 and 11.2 branches, respectively.
