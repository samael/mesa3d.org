---
title:    "Mesa 19.0.4 is released"
date:     2019-05-09
category: releases
tags:     []
---
[Mesa 19.0.4](https://docs.mesa3d.org/relnotes/19.0.4.html) is released. This is a bug-fix
release.
