---
title:    "Mesa 8.0.4 is released"
date:     2012-07-10
category: releases
tags:     []
---
[Mesa 8.0.4](https://docs.mesa3d.org/relnotes/8.0.4.html) is released. This is a bug fix
release.
