---
title:    "Mesa 22.0.2 is released"
date:     2022-04-21
category: releases
tags:     []
---
[Mesa 22.0.2](https://docs.mesa3d.org/relnotes/22.0.2.html) is released. This is a bug fix release.
