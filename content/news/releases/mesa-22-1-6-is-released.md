---
title:    "Mesa 22.1.6 is released"
date:     2022-08-10
category: releases
tags:     []
---
[Mesa 22.1.6](https://docs.mesa3d.org/relnotes/22.1.6.html) is released.
This is a bug fix release.
