---
title:    "Mesa 23.1.8 is released"
date:     2023-09-20
category: releases
tags:     []
---
[Mesa 23.1.8](https://docs.mesa3d.org/relnotes/23.1.8.html) is released.
This is a bug fix release.
