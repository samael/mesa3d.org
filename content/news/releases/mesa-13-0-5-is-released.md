---
title:    "Mesa 13.0.5 is released"
date:     2017-02-20
category: releases
tags:     []
---
[Mesa 13.0.5](https://docs.mesa3d.org/relnotes/13.0.5.html) is released. This is a bug-fix
release.
