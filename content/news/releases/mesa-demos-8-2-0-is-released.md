---
title:    "Mesa demos 8.2.0 is released"
date:     2014-07-05
category: releases
tags:     [mesa-demos]
---
Mesa demos 8.2.0 is released. See the
[announcement](https://lists.freedesktop.org/archives/mesa-announce/2014-July/000100.html)
for more information about the release. You can download it from
[ftp.freedesktop.org/pub/mesa/demos/8.2.0/](ftp://ftp.freedesktop.org/pub/mesa/demos/8.2.0/).
