---
title:    "Mesa 22.0.3 is released"
date:     2022-05-04
category: releases
tags:     []
---
[Mesa 22.0.3](https://docs.mesa3d.org/relnotes/22.0.3.html) is released.
This is a bug fix release.
