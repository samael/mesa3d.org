---
title:    "Mesa 18.3.1 is released"
date:     2018-12-11
category: releases
tags:     []
---
[Mesa 18.3.1](https://docs.mesa3d.org/relnotes/18.3.1.html) is released. This is a bug-fix
release.
