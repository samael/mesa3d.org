---
title:    "Mesa 23.1.9 is released"
date:     2023-10-04
category: releases
tags:     []
---
[Mesa 23.1.9](https://docs.mesa3d.org/relnotes/23.1.9.html) is released.
This is a bug fix release.
