---
title:    "Mesa 7.8.1 is released"
date:     2010-04-05
category: releases
tags:     []
---
[Mesa 7.8.1](https://docs.mesa3d.org/relnotes/7.8.1.html) is released. This is a bug-fix
release for a few critical issues in the 7.8 release.
