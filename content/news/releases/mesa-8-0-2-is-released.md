---
title:    "Mesa 8.0.2 is released"
date:     2012-03-21
category: releases
tags:     []
---
[Mesa 8.0.2](https://docs.mesa3d.org/relnotes/8.0.2.html) is released. This is a bug fix
release.
