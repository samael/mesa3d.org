---
title:    "Mesa 9.0.1 is released"
date:     2012-11-16
category: releases
tags:     []
---
[Mesa 9.0.1](https://docs.mesa3d.org/relnotes/9.0.1.html) is released. This is a bug fix
release.
