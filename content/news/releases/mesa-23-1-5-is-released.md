---
title:    "Mesa 23.1.5 is released"
date:     2023-08-02
category: releases
tags:     []
---
[Mesa 23.1.5](https://docs.mesa3d.org/relnotes/23.1.5.html) is released.
This is a bug fix release.
