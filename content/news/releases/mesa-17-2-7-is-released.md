---
title:    "Mesa 17.2.7 is released"
date:     2017-12-14
category: releases
tags:     []
---
[Mesa 17.2.7](https://docs.mesa3d.org/relnotes/17.2.7.html) is released. This is a bug-fix
release.
