---
Title: Developers
---
Both professional and volunteer developers contribute to Mesa.

Some of the companies that has made significant contributions
include (sorted alphabetically):

- [AMD]
- [Collabora]
- [Google]
- [Igalia]
- [Intel]
- [Red Hat]
- [Valve]
- [VMware]

## Contractors

The following companies can be contacted for custom Mesa / 3D graphics
development (sorted alphabetically):

- [Collabora]
- [Igalia]
- [LunarG]

## Volunteers

Volunteers have made significant contributions to all parts of Mesa,
including complete device drivers.

There's really too many to mention here, and the list keeps on changing
but you can see an up-to-date list of significant contributors on our
GitLab [here][contributors].

[AMD]: https://www.amd.com
[Collabora]: https://www.collabora.com
[Google]: https://opensource.google
[Igalia]: https://www.igalia.com
[Intel]: https://01.org/linuxgraphics
[Red Hat]: https://www.redhat.com
[Valve]: https://www.valvesoftware.com
[VMware]: https://www.vmware.com
[LunarG]: https://www.lunarg.com
[contributors]: https://gitlab.freedesktop.org/mesa/mesa/-/graphs/main?ref_type=heads
